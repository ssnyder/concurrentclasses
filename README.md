This repository contains copies of some of the concurrent data structure classes
used in the ATLAS software.  The original code is from the ATLAS Athena
repository <https://gitlab.cern.ch/atlas/athena>, mostly from the
Control/CxxUtils directory 
(<https://gitlab.cern.ch/atlas/athena/tree/main/Control/CxxUtils>).
See there for the history of the code.

Some of these classes are discussed in the CHEP 19 proceedings
[Concurrent data structures in the {ATLAS} offline software](https://doi.org/10.1051/epjconf/202024505007).

A CMake file is provided to build and install the library and run the tests.
The build was tested on EL8 (x86_64) and EL9 (x86_64/aarch64).  Other platforms
may also work as long as the compiler supports at least c++17.

The example directory gives a very simple example of a program building
against the library.
