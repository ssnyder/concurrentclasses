
include(CMakeFindDependencyMacro)
include(FindPackageHandleStandardArgs)
find_package(Threads)

# - Include the targets file to create the imported targets that a client can
# link to (libraries) or execute (programs)
include("${CMAKE_CURRENT_LIST_DIR}/ConcurrentClassesTargets.cmake")
get_filename_component(PACKAGE_PREFIX_DIR "${CMAKE_CURRENT_LIST_DIR}/../../../"
  ABSOLUTE)

set( ConcurrentClasses_INCLUDE_DIRS ${PACKAGE_PREFIX_DIR}/include ${PACKAGE_PREFIX_DIR}/include/ConcurrentClasses )

get_property(TEST_CONCURRENTCLASSES_LIBRARY TARGET ConcurrentClasses::ConcurrentClasses PROPERTY LOCATION)
find_package_handle_standard_args(ConcurrentClasses  DEFAULT_MSG CMAKE_CURRENT_LIST_FILE TEST_CONCURRENTCLASSES_LIBRARY)
