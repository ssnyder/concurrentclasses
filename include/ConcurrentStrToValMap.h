// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file ConcurrentClasses/ConcurrentStrToValMap.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Aug, 2023
 * @brief ConcurrentStrToValMap wrapper header.
 */


#ifndef CONCURRENTCLASSES_CONCURRENTSTRTOVALMAP_H
#define CONCURRENTCLASSES_CONCURRENTSTRTOVALMAP_H


#include "ConcurrentClasses/CxxUtils/ConcurrentStrToValMap.h"


namespace ConcurrentClasses {


using CxxUtils::ConcurrentStrToValMap;


} // namespace ConcurrentClasses


#endif // not CONCURRENTCLASSES_CONCURRENTSTRTOVALMAP_H
