// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file ConcurrentClasses/ConcurrentPtrSet.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Aug, 2023
 * @brief ConcurrentPtrSet wrapper header.
 */


#ifndef CONCURRENTCLASSES_CONCURRENTPTRSET_H
#define CONCURRENTCLASSES_CONCURRENTPTRSET_H


#include "ConcurrentClasses/CxxUtils/ConcurrentPtrSet.h"


namespace ConcurrentClasses {


using CxxUtils::ConcurrentPtrSet;


} // namespace ConcurrentClasses


#endif // not CONCURRENTCLASSES_CONCURRENTPTRSET_H
