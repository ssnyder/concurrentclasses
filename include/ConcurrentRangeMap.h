// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file ConcurrentClasses/ConcurrentRangeMap.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Aug, 2023
 * @brief ConcurrentRangeMap wrapper header.
 */


#ifndef CONCURRENTCLASSES_CONCURRENTRANGEMAP_H
#define CONCURRENTCLASSES_CONCURRENTRANGEMAP_H


#include "ConcurrentClasses/CxxUtils/ConcurrentRangeMap.h"


namespace ConcurrentClasses {


using CxxUtils::IRangeMapPayloadDeleter;
using CxxUtils::ConcurrentRangeMap;


} // namespace ConcurrentClasses


#endif // not CONCURRENTCLASSES_CONCURRENTRANGEMAP_H
