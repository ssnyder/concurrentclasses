// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file ConcurrentClasses/SimpleUpdater.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Aug, 2023
 * @brief SimpleUpdater wrapper header.
 */


#ifndef CONCURRENTCLASSES_SIMPLEUPDATER_H
#define CONCURRENTCLASSES_SIMPLEUPDATER_H


#include "ConcurrentClasses/CxxUtils/SimpleUpdater.h"


namespace ConcurrentClasses {


using CxxUtils::SimpleUpdater;


} // namespace ConcurrentClasses


#endif // not CONCURRENTCLASSES_SIMPLEUPDATER_H
