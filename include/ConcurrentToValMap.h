// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file ConcurrentClasses/ConcurrentToValMap.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Aug, 2023
 * @brief ConcurrentToValMap wrapper header.
 */


#ifndef CONCURRENTCLASSES_CONCURRENTTOVALMAP_H
#define CONCURRENTCLASSES_CONCURRENTTOVALMAP_H


#include "ConcurrentClasses/CxxUtils/ConcurrentToValMap.h"


namespace ConcurrentClasses {


using CxxUtils::ConcurrentToValMap;


} // namespace ConcurrentClasses


#endif // not CONCURRENTCLASSES_CONCURRENTTOVALMAP_H
