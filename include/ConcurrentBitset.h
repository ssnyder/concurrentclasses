// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file ConcurrentClasses/ConcurrentBitset.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Aug, 2023
 * @brief ConcurrentBitset wrapper header.
 */


#ifndef CONCURRENTCLASSES_CONCURRENTBITSET_H
#define CONCURRENTCLASSES_CONCURRENTBITSET_H


#include "ConcurrentClasses/CxxUtils/ConcurrentBitset.h"


namespace ConcurrentClasses {


using CxxUtils::ConcurrentBitset;


} // namespace ConcurrentClasses


#endif // not CONCURRENTCLASSES_CONCURRENTBITSET_H
