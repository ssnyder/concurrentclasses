// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file ConcurrentClasses/ConcurrentMap.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Aug, 2023
 * @brief ConcurrentMap wrapper header.
 */


#ifndef CONCURRENTCLASSES_CONCURRENTMAP_H
#define CONCURRENTCLASSES_CONCURRENTMAP_H


#include "ConcurrentClasses/CxxUtils/ConcurrentMap.h"


namespace ConcurrentClasses {


using CxxUtils::ConcurrentMap;


} // namespace ConcurrentClasses


#endif // not CONCURRENTCLASSES_CONCURRENTMAP_H
