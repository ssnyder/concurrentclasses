// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file ConcurrentClasses/ConcurrentStrMap.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Aug, 2023
 * @brief ConcurrentStrMap wrapper header.
 */


#ifndef CONCURRENTCLASSES_CONCURRENTSTRMAP_H
#define CONCURRENTCLASSES_CONCURRENTSTRMAP_H


#include "ConcurrentClasses/CxxUtils/ConcurrentStrMap.h"


namespace ConcurrentClasses {


using CxxUtils::ConcurrentStrMap;


} // namespace ConcurrentClasses


#endif // not CONCURRENTCLASSES_CONCURRENTSTRMAP_H
