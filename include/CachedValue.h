// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file ConcurrentClasses/CachedValue.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Aug, 2023
 * @brief CachedValue wrapper header.
 */


#ifndef CONCURRENTCLASSES_CACHEDVALUE_H
#define CONCURRENTCLASSES_CACHEDVALUE_H


#include "ConcurrentClasses/CxxUtils/CachedValue.h"


namespace ConcurrentClasses {


using CxxUtils::CachedValue;


} // namespace ConcurrentClasses


#endif // not CONCURRENTCLASSES_CACHEDVALUE_H
