// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file ConcurrentClasses/CachedPointer.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Aug, 2023
 * @brief CachedPointer wrapper header.
 */


#ifndef CONCURRENTCLASSES_CACHEDPOINTER_H
#define CONCURRENTCLASSES_CACHEDPOINTER_H


#include "ConcurrentClasses/CxxUtils/CachedPointer.h"


namespace ConcurrentClasses {


using CxxUtils::CachedPointer;


} // namespace ConcurrentClasses


#endif // not CONCURRENTCLASSES_CACHEDPOINTER_H
