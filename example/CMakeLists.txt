cmake_minimum_required( VERSION 3.10 )
project( ConcurrentClassesExample )
find_package( ConcurrentClasses )

# Minimum required.  Should also work with C++20 / 23.
set( CMAKE_CXX_STANDARD 17 )

include_directories( cc_example ${ConcurrentClasses_INCLUDE_DIRS} )
link_libraries( ConcurrentClasses::ConcurrentClasses )

add_executable( cc_example cc_example.cxx )
