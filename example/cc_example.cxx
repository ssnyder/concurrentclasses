#include "ConcurrentClasses/ConcurrentStrMap.h"
#include "ConcurrentClasses/SimpleUpdater.h"
#include <cassert>
#include <iostream>


void strMapExample()
{
  using Map_t = ConcurrentClasses::ConcurrentStrMap<int, ConcurrentClasses::SimpleUpdater>;
  Map_t map {Map_t::Updater_t()};

  {
    auto [it, flag] = map.emplace ("foo", 1);
    assert (flag);
    assert (it->first == "foo");
  }
  {
    auto [it, flag] = map.emplace ("bar", 2);
    assert (flag);
    assert (it->first == "bar");
  }

  assert (map.find("foo")->second == 1);
  assert (map.find("bar")->second == 2);
  assert (map.find("fee") == map.end());

  for (const auto p: map)
    std::cout << p.first << " " << p.second << std::endl;
}


int main()
{
  strMapExample();
  return 0;
}
